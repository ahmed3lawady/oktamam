<?php

namespace App\Models;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    protected $rules = [
        'name' => 'required|max:255'
    ];

    protected $createRules = [
        'email' => 'required|email|unique:users', 'password' => 'required|min:3'
    ];

    protected $updateRules = [
        'password' => 'required|min:3'
    ];

    function createRules()
    {
        return array_merge($this->rules, $this->createRules);
    }

    function updateRules()
    {
        if(\Request::has('password'))
            return array_merge($this->rules, $this->updateRules);
        return $this->rules;
    }
}
