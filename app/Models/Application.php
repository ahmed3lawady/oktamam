<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Application extends Model
{
    protected $fillable = [
        'code', 'firstname', 'middlename', 'surname', 'photo', 'birthdate',
        'nationality', 'birth_country', 'gender', 'passport', 'grade'
    ];

    public static $rules = [
        'firstname' => 'required', 'middlename' => 'required', 'surname' => 'required', 'birthdate' => 'required|date'
    ];

    public function fullname()
    {
        return $this->firstname. ' ' . $this->middelname . ' ' . $this->surname;
    }

    static function getRules()
    {
        $rules = [
            'firstname' => 'required', 'middlename' => 'required', 'surname' => 'required', 'birthdate' => 'required|date'
        ];
        if(\Request::hasFile('photo'))
            $rules['photo'] = 'required|image|mimes:jpeg,bmp,png';

        if(\Request::hasFile('passport'))
            $rules['passport'] = 'required|image|mimes:jpeg,bmp,png';

        if(\Request::hasFile('certificates'))
            $rules['certificates.*'] = 'required|image|mimes:jpeg,bmp,png';

        return $rules;
    }

    public function certificates()
    {
        return $this->hasMany('App\Models\Certificate', 'application_id');
    }
}
