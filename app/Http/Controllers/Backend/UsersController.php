<?php

namespace App\Http\Controllers\Backend;

use App\Models\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;

class UsersController extends Controller
{
    protected $data = [];
    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $this->data['allUsers'] = User::all();
        return view('backend.users.list', $this->data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $this->data['title'] = trans('users.create');
        return view('backend.users.form', $this->data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $user              = new User();
        $user->name        = $request->name;
        $user->email       = $request->email;
        $user->password    = Hash::make($request->password);

        $this->validate($request, $user->createRules());

        if($user->save()){
            return back()->with('msg', trans('common.add-success'));
        }else{
            return back()->with('msg', trans('common.add-failed'));
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $this->data['title'] = trans('users.update');
        $this->data['userData'] = User::find($id);
        return view('backend.users.form', $this->data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $user              = User::find($id);
        $user->name        = $request->name;
        if($request->has('password')) {
            $user->password = Hash::make($request->password);
        }

        $this->validate($request, $user->updateRules());

        if($user->save()){
            return back()->with('msg', trans('common.update-success'));
        }else{
            return back()->with('msg', trans('common.update-failed'));
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if(User::destroy($id)){
            return back()->with('msg', trans('common.delete-success'));
        }else{
            return back()->with('msg', trans('common.delete-failed'));
        }
    }
}
