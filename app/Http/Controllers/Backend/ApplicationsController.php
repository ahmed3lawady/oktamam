<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use App\Models\Application;
use App\Models\Certificate;
use App\Models\Country;
use Illuminate\Http\Request;

class ApplicationsController extends Controller
{
    protected $data = [];
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $this->data['allApps'] = Application::all();
        return view('backend.applications.list', $this->data);
    }

    public function appsList()
    {
        $allApps = Application::with('certificates')->get();
        return response(['success' => true, 'apps' => json_encode($allApps)]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $this->data['title'] = trans('applications.create');
        $this->data['countries'] = Country::all();
        return view('backend.applications.form', $this->data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, Application::getRules());

        $app                = new Application();
        $app->code          = md5(uniqid(rand(), true));
        $app->firstname     = $request->firstname;
        $app->middlename    = $request->middlename;
        $app->surname       = $request->surname;
        $app->birthdate     = $request->birthdate;
        $app->nationality   = $request->nationality;
        $app->birth_country = $request->birth_country;
        $app->gender        = $request->gender;
        $app->grade         = $request->grade;

        if ($request->hasFile('photo')) {
            $app->photo = $this->upload($request);
        }
        if ($request->hasFile('passport')) {
            $app->passport = $this->upload($request, 'passport');
        }
        if($app->save()){
            if ($request->hasFile('certificates')){
                $files = [];
                foreach ($request->file('certificates') as $file){
                    $fileName = rand(11111,99999).'.'.$file->getClientOriginalExtension();
                    $file->move(storage_path('/upload/'), $fileName);
                    \Image::make(storage_path('/upload/')."/".$fileName)->resize(250, 250)->blur()->save(storage_path('/upload/')."/small-".$fileName);
                    $files[] = new \App\Models\Certificate(['photo' => $fileName]);
                }
                $app->certificates()->saveMany($files);
            }
            return back()->with('msg', trans('common.add-success'));
        }else{
            return back()->with('msg', trans('common.add-failed'));
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $app = Application::with('certificates')->find($id);
        if(!$app)
            return response(['success' => false]);

        return response(['success' => true, 'app' => json_encode($app)]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $this->data['title'] = trans('applications.update');
        $this->data['appData'] = Application::find($id);
        $this->data['countries'] = Country::all();
        return view('backend.applications.form', $this->data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, Application::getRules());

        $app                = Application::find($id);
        $app->firstname     = $request->firstname;
        $app->middlename    = $request->middlename;
        $app->surname       = $request->surname;
        $app->birthdate     = $request->birthdate;
        $app->nationality   = $request->nationality;
        $app->birth_country = $request->birth_country;
        $app->gender        = $request->gender;
        $app->grade         = $request->grade;

        if ($request->hasFile('photo')) {
            $app->photo = $this->upload($request);
        }
        if ($request->hasFile('passport')) {
            $app->passport = $this->upload($request, 'passport');
        }

        if($app->save()){
            if ($request->hasFile('certificates')){
                $files = [];
                foreach ($request->file('certificates') as $file){
                    $fileName = rand(11111,99999).'.'.$file->getClientOriginalExtension();
                    $file->move(storage_path('/upload/'), $fileName);
                    \Image::make(storage_path('/upload/')."/".$fileName)->resize(250, 250)->blur()->save(storage_path('/upload/')."/small-".$fileName);
                    $files[] = new \App\Models\Certificate(['photo' => $fileName]);
                }
                $app->certificates()->saveMany($files);
            }
            return back()->with('msg', trans('common.update-success'));
        }else{
            return back()->with('msg', trans('common.update-failed'));
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $appData = Application::find($id);
        $app = Application::destroy($id);
        if($app){
            Certificate::where('application_id', $id)->delete();
            if(!empty($appData->photo)){
                $this->deletePhoto($appData->photo);
            }
            if(!empty($appData->passport)){
                $this->deletePhoto($appData->passport);
            }
            return back()->with('msg', trans('common.delete-success'));
        }else{
            return back()->with('msg', trans('common.delete-failed'));
        }
    }

    function upload($request, $file = 'photo'){
        $destinationPath = storage_path('/upload/');
        $extension = $request->file($file)->getClientOriginalExtension();
        $fileName = rand(11111,99999).'.'.$extension;
        $request->file($file)->move($destinationPath, $fileName);
        \Image::make($destinationPath."/".$fileName)->resize(150, 150)->blur()->save($destinationPath."/thumbnail-".$fileName);
        \Image::make($destinationPath."/".$fileName)->resize(250, 250)->blur()->save($destinationPath."/small-".$fileName);
        \Image::make($destinationPath."/".$fileName)->resize(400, 350)->blur()->save($destinationPath."/medium-".$fileName);
        \Image::make($destinationPath."/".$fileName)->resize(800, 600)->blur()->save($destinationPath."/large-".$fileName);
        return $fileName;
    }

    function deletePhoto($filename)
    {
        \File::delete(storage_path('/upload/'.$filename));
        \File::delete(storage_path('/upload/thumbnail-'.$filename));
        \File::delete(storage_path('/upload/small-'.$filename));
        \File::delete(storage_path('/upload/medium-'.$filename));
        \File::delete(storage_path('/upload/large-'.$filename));
    }

    function getImage($filename)
    {
        $path = storage_path('upload/'.$filename);
        if(!\File::exists($path)){
            $path = 'backend/img/no-image-found.jpg';
        }
        $file = \File::get($path);
        $type = \File::mimeType($path);
        return \Response::make($file, 200)->header("Content-Type", $type);
    }
}
