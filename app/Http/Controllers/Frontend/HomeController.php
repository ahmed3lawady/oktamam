<?php

namespace App\Http\Controllers\Frontend;

use App\Http\Controllers\Controller;
use App\Models\Application;
use App\Models\Country;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if($request->isMethod('post') && $request->ajax()){
            \Validator::make($request->all(), Application::getRules())->validate();

            $app                = new Application();
            $app->code          = md5(uniqid(rand(), true));
            $app->firstname     = $request->firstname;
            $app->middlename    = $request->middlename;
            $app->surname       = $request->surname;
            $app->birthdate     = $request->birthdate;
            $app->nationality   = $request->nationality;
            $app->birth_country = $request->birth_country;
            $app->gender        = $request->gender;
            $app->grade         = $request->grade;

            if ($request->hasFile('photo')) {
                $app->photo = $this->upload($request);
            }
            if ($request->hasFile('passport')) {
                $app->passport = $this->upload($request, 'passport');
            }

            if($app->save()){
                if ($request->hasFile('certificates')){
                    $files = [];
                    foreach ($request->file('certificates') as $file){
                        $fileName = rand(11111,99999).'.'.$file->getClientOriginalExtension();
                        $file->move(storage_path('/upload/'), $fileName);
                        \Image::make(storage_path('/upload/')."/".$fileName)->resize(250, 250)->blur()->save(storage_path('/upload/')."/small-".$fileName);
                        $files[] = new \App\Models\Certificate(['photo' => $fileName]);
                    }
                    $app->certificates()->saveMany($files);
                }
                return response(['success' => true, 'msg' => trans('common.add-success')], 200);
            }else{
                return response(['success' => false, 'errors' => json_encode(['msg' => trans('common.add-failed')])], 400);
            }
        }

        $countries = Country::all();
        return view('frontend.home', ['countries' => $countries]);
    }

    function upload($request, $file = 'photo'){
        $destinationPath = storage_path('/upload/');
        $extension = $request->file($file)->getClientOriginalExtension();
        $fileName = rand(11111,99999).'.'.$extension;
        $request->file($file)->move($destinationPath, $fileName);
        \Image::make($destinationPath."/".$fileName)->resize(150, 150)->blur()->save($destinationPath."/thumbnail-".$fileName);
        \Image::make($destinationPath."/".$fileName)->resize(250, 250)->blur()->save($destinationPath."/small-".$fileName);
        \Image::make($destinationPath."/".$fileName)->resize(400, 350)->blur()->save($destinationPath."/medium-".$fileName);
        \Image::make($destinationPath."/".$fileName)->resize(800, 600)->blur()->save($destinationPath."/large-".$fileName);
        return $fileName;
    }
}
