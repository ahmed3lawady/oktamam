(function ($) {
    "use strict";
    var Social = function (options) { this.init('socialmedia', options, Social.defaults); };

    $.fn.editableutils.inherit(Social, $.fn.editabletypes.abstractinput);
    $.extend(Social.prototype, {
        render: function() { this.$input = this.$tpl.find('input'); },
        value2str: function(value) {
            var str = '';
            if(value) {
                for(var k in value) { str = str + k + ':' + value[k] + ';'; }
            }
            return str;
        },
        str2value: function(str) { return str; },
        value2input: function(value) {
            if(!value) { return; }
            this.$input.filter('[name="name"]').val(value.name);
        },
        input2value: function() {
            return {
                name: this.$input.filter('[name="name"]').val(),
            };
        },
        activate: function() { this.$input.filter('[name="name"]').focus(); }
    });
    Social.defaults = $.extend({}, $.fn.editabletypes.abstractinput.defaults, {
        tpl: '<div class="editable-social"><label><span>name: </span><input type="text" name="name" class="input-small"></label></div>'+
             '<div class="editable-social"><label><span>age: </span><input type="text" name="age" class="input-small"></label></div>',
        inputclass: ''
    });
    $.fn.editabletypes.social = Social;
}(window.jQuery));