$(document).ready(function() {

  "use strict";

  // -------------- Preloader -------------- 
  $(".preloader").addClass('animated fadeOut');
  setTimeout(function(){
    $(".preloader").addClass('loaded');
  }, 2500);

  // -------------- Wow -------------- 

  var wow = new WOW(
  {
    animateClass: 'animated',
    offset: 100,
    mobile: false
  }
  );
  wow.init();

});

