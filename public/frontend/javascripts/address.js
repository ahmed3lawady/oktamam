/**
Address editable input.
Internally value stored as {city: "Moscow", street: "Lenina", building: "15"}

@class address
@extends abstractinput
@final
@example
<a href="#" id="address" data-type="address" data-pk="1">awesome</a>
<script>
$(function(){
    $('#address').editable({
        url: '/post',
        title: 'Enter city, street and building #',
        value: {
            city: "Moscow", 
            street: "Lenina", 
            building: "15"
        }
    });
});
</script>
**/
(function ($) {
    "use strict";
    var Address = function (options) { this.init('address', options, Address.defaults); };

    //inherit from Abstract input
    $.fn.editableutils.inherit(Address, $.fn.editabletypes.abstractinput);
    $.extend(Address.prototype, {
        //Renders input from tpl
        render: function() {
            this.$input = this.$tpl.find('input');
            this.$select = this.$tpl.find('select');
            $('body').on('change', '#cityId', function(){
                var selectedCity = $('#cityId').val();
                $.get("/getCityRegions/"+selectedCity, function(data, status){
                    var opt = '';
                    $.each(data, function (index, value) { opt += '<option label="'+value+'" value="'+index+'">'+value+'</option>'; });
                    $('#regionId').html(opt);
                });
            });
        },
        //Default method to show value in element. Can be overwritten by display option.
        value2html: function(value, element) {
            if(!value) {
                $(element).empty();
                return; 
            }
            var html = $('<div>').text(value.address).html() + ', ' + $('<div>').text(this.$select.filter('[name="region_id"]').find("option:selected").text()).html() + ', ' + $('<div>').text(this.$select.filter('[name="city_id"]').find("option:selected").text()).html();

            $(element).html(html);
        },
        //Gets value from element's html
        html2value: function(html) {
            /*
            you may write parsing method to get value by element's html
            e.g. "Moscow, st. Lenina, bld. 15" => {city: "Moscow", street: "Lenina", building: "15"}
            but for complex structures it's not recommended.
            Better set value directly via javascript, e.g.
            editable({ value: { city: "Moscow", street: "Lenina", building: "15" } });
            */
            return null;
        },
        //Converts value to string. It is used in internal comparing (not for sending to server).
        value2str: function(value) {
            var str = '';
            if(value) {
               for(var k in value) { str = str + k + ':' + value[k] + ';'; }
            }
            return str;
        },
        //Converts string to value. Used for reading value from 'data-value' attribute.
        str2value: function(str) {
            //this is mainly for parsing value defined in data-value attribute. If you will always set value by javascript, no need to overwrite it
            return str;
        },
        //Sets value of input.
        value2input: function(value) {
            $.get("/getCities", function(data, status){
                var opt = '';
                $.each(data, function (index, val) {
                    if(value != null && value != undefined && value.city_id == index)
                        opt += '<option label="'+val+'" value="'+index+'" selected="selected">'+val+'</option>';
                    else
                        opt += '<option label="'+val+'" value="'+index+'">'+val+'</option>';
                });
                $('#cityId').html(opt);
            });
            if(!value) { return; }
            this.$input.filter('[name="address"]').val(value.address);
            //this.$select.filter('[name="city_id"]').val(value.city_id);
            //this.$select.filter('[name="region_id"]').val(value.region_id);
            $.get("/getCityRegions/"+value.city_id, function(data, status){
                var opt = '';
                $.each(data, function (index, val) {
                    if(value.region_id == index)
                        opt += '<option label="'+val+'" value="'+index+'" selected="selected">'+val+'</option>';
                    else
                        opt += '<option label="'+val+'" value="'+index+'">'+val+'</option>';
                });
                $('#regionId').html(opt);
            });
        },
        //Returns value of input.
        input2value: function() {
            return {
                address: this.$input.filter('[name="address"]').val(),
                region_id: this.$select.filter('[name="region_id"]').val(),
                city_id: this.$select.filter('[name="city_id"]').val()
            };
        },
        //Activates input: sets focus on the first field.
        activate: function() { this.$input.filter('[name="address"]').focus(); },
        //Attaches handler to submit form in case of 'showbuttons=false' mode
        autosubmit: function() {
            this.$input.keydown(function (e) {
                if (e.which === 13) { $(this).closest('form').submit(); }
            });
        }
    });

    Address.defaults = $.extend({}, $.fn.editabletypes.abstractinput.defaults, {
        tpl: '<div class="editable-address"><label><span>'+$('#address').attr('data-addressTitle')+': </span><input type="text" name="address" class="form-control input-sm"></label></div>'+
             '<div class="editable-address"><label><span>'+$('#address').attr('data-cityTitle')+': </span><select name="city_id" id="cityId" class="form-control input-sm"></select></label></div>'+
             '<div class="editable-address"><label><span>'+$('#address').attr('data-regionTitle')+': </span><select name="region_id" id="regionId" class="form-control input-sm"></select></label></div>',
        inputclass: ''
    });
    $.fn.editabletypes.address = Address;
}(window.jQuery));