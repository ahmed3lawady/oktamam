<?php
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of the routes that are handled
| by your application. Just tell Laravel the URIs it should respond
| to using a Closure or controller method. Build something great!
|
*/

Route::group(array('prefix' => 'admin'), function() {
    Auth::routes();
    Route::get('logout', ['as' => 'logout', 'uses' => '\App\Http\Controllers\Auth\LoginController@logout']); //Just added to fix issue

    Route::group(['middleware' => 'auth', 'namespace' => 'Backend'], function () {

        Route::group(array('prefix' => 'image'), function() {
            Route::get('{filename}', ['as' => 'image', 'uses' => 'ApplicationsController@getImage']);
            Route::get('large-{filename}', ['as' => 'image.large', 'uses' => 'ApplicationsController@getImage']);
            Route::get('medium-{filename}', ['as' => 'image.medium', 'uses' => 'ApplicationsController@getImage']);
            Route::get('small-{filename}', ['as' => 'image.small', 'uses' => 'ApplicationsController@getImage']);
            Route::get('thumbnail-{filename}', ['as' => 'image.thumbnail', 'uses' => 'ApplicationsController@getImage']);
        });

        Route::resource('users', 'UsersController');
        Route::resource('apps', 'ApplicationsController');
        Route::get('/', function (){return redirect('admin/apps');});
    });

});

Route::match(['get', 'post'], '/', ['as' => 'home', 'uses' => 'Frontend\HomeController@index']);
