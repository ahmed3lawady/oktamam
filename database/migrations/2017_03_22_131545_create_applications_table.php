<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateApplicationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('applications', function (Blueprint $table) {
            $table->increments('id');
            $table->string('code');
            $table->string('firstname');
            $table->string('middlename');
            $table->string('surname');
            $table->string('photo')->nullable();
            $table->date('birthdate');
            $table->integer('nationality');
            $table->integer('birth_country');
            $table->enum('gender', ['male', 'female']);
            $table->string('passport')->nullable();
            $table->string('grade')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('applications');
    }
}