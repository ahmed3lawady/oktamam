<?php

return array(
    'applications' 		=> 'Applications',
    'code'              => 'Code',
    'user'			    => 'User',
    'view-list' 	    => 'View Applications',
    'list' 	            => 'Applications List',
    'create'            => 'Create New Application',
    'update'	        => 'Update Application',
    'fullname'  		=> 'Full Name',
    'firstname'  		=> 'First Name',
    'middelname'  		=> 'Middel Name',
    'surname'  		    => 'Surname',
    'email'			    => 'E-Mail',
    'image'			    => 'Personal Photo',
    'passport'          => 'Passport Image',
    'password'		    => 'Password',
    'repassword'	    => 'Password Confirm',
    'birthdate'	        => 'Date of Birth',
    'birth_country'     => 'Country of Birth',
    'nationality'       => 'Nationality',
    'gender'            => [
        'gender' => 'Gender', 'male' => 'Male', 'female' => 'Female'
    ],
    'grade'             => 'Graduation Degree',
    'certificates'      => 'Graduation Certificates Photos',
    'update-password'   => 'Leave Password field empty for no Changes!',
    'reset-password'    => 'Reset your Password',
    'currpassword'      => 'Current Password',
    'reset-password-success' => '<div class="alert alert-success">Your Password Updated...</div>',
    'reset-password-error'   => '<div class="alert alert-danger">Error! Your Password did not changed...</div>',
    'field-required'    => 'This field is required',
);
