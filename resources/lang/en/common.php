<?php

return array(
    'cpanel'                    => 'Control Panel',
    'update'                    => 'Update',
    'delete'                    => 'Delete',
    'save'                      => 'Save',
    'add-success'               => '<div class="callout callout-success">Item has been Created Successfully...</div>',
    'add-failed'                => '<div class="callout callout-danger">Error! Create Failed, Please Try again later...</div>',
    'add-failed-front'          => '<div class="alert alert-danger">Error! Create Failed, Please Try again later...</div>',
    'update-success'            => '<div class="callout callout-success">Item has been Updated Successfully...</div>',
    'update-failed'             => '<div class="callout callout-danger">Error! Update Failed, Please Try again later...</div>',
    'delete-success'            => '<div class="callout callout-success">Item has been Deleted Successfuly...</div>',
    'delete-failed'             => '<div class="callout callout-danger">Error! Delete Failed, Please Try again later...</div>',
    'delete-confirm'            => 'Are you sure form Deleting this Item?',
    'none'                      => 'None',
    'enter-ur'                  => 'Enter your ',
    'no-result'                 => 'No Results Found!',
    'message-alert'             => '<div class="alert alert-:alert">:msg</div>',
);
