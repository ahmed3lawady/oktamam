<!DOCTYPE html>
<html lang="en">
@include('frontend.partials._head')
<body>
    <div class="preloader">
        <div class="image-container">
            <div class="image"><img src="{{ asset('frontend/images/preloader.gif')}}" alt=""></div>
        </div>
    </div>

    <section class="resume-form sec-hq-pad-t sec-hq-pad-b animated wow fadeIn" data-wow-delay="0.2s">
        <div class="form-content">
            <div class="container">
                @if(Session::has('msg')){!! Session::get('msg') !!}@endif
                @if(!empty($errors->all()))
                    <ul class="alert alert-danger">
                        @foreach($errors->all('<li>:message</li>') as $message) {!! $message !!}  @endforeach
                    </ul>
                @endif
                {{ Form::open(['class' => 'form-horizontal', 'id' => 'applicationForm', 'files'=> true]) }}
                <div class="personal-data col-md-12">
                    <div class="col-md-12">
                        <div class="title-underlined"><h3>{{ trans('applications.create') }}</h3></div>
                    </div>
                    <div class="col-md-3 col-sm-3 col-xs-12">
                        <strong>{{ trans('applications.image') }}</strong>
                        <div class="user-photo-wrap valign-wrap">
                            <div class="user-photo valign-middle"><img src="{{ asset('frontend/images/profile-male.png') }}" class="pp"></div>
                            <input type="file" name="photo" class="upload">
                        </div>

                        <strong>{{ trans('applications.passport') }}</strong>
                        <div class="user-photo-wrap valign-wrap" style="border-radius: 0;">
                            <div class="user-photo valign-middle" style="border-radius: 0;"><img src="{{ asset('frontend/images/passport.png') }}" class="pp"></div>
                            <input type="file" name="passport" class="upload">
                        </div>
                    </div>
                    <div class="col-md-9 col-sm-9">
                        <div class="col-md-4 col-sm-4">
                            <div class="form-group">
                                <div class="col-md-12 col-sm-12"><strong>{{ trans('applications.firstname') }}*</strong></div>
                                <div class="col-md-12 col-sm-12">
                                    <input type="text" name="firstname" class="def-input" placeholder="{{ trans('applications.firstname') }}" required>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-4 col-sm-4">
                            <div class="form-group">
                                <div class="col-md-12 col-sm-12"><strong>{{ trans('applications.middelname') }}*</strong></div>
                                <div class="col-md-12 col-sm-12">
                                    <input type="text" name="middlename" class="def-input" placeholder="{{ trans('applications.middelname') }}" required>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-4 col-sm-4">
                            <div class="form-group">
                                <div class="col-md-12 col-sm-12"><strong>{{ trans('applications.surname') }}*</strong></div>
                                <div class="col-md-12 col-sm-12">
                                    <input type="text" name="surname" class="def-input" placeholder="{{ trans('applications.surname') }}" required>
                                </div>
                            </div>
                        </div>

                        <div class="col-md-6 col-sm-6">
                            <div class="form-group">
                                <div class="col-md-12"><strong>{{ trans('applications.birthdate') }}*</strong></div>
                                <div class="col-md-12">
                                    <input type="text" name="birthdate" class="def-input datepicker" placeholder="{{ trans('applications.birthdate') }}" required>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6 col-sm-6">
                            <div class="form-group">
                                <div class="col-md-12"><strong>{{ trans('applications.birth_country') }}</strong></div>
                                <div class="col-md-12">
                                    <select name="birth_country" class="def-input def-select countries">
                                        @if(count($countries))
                                            @foreach($countries as $country)
                                                <option value="{{$country->id}}">{{ $country->name_en }}</option>
                                            @endforeach
                                        @endif
                                    </select>
                                </div>
                            </div>
                        </div>

                        <div class="col-md-6 col-sm-6">
                            <div class="form-group">
                                <div class="col-md-12"><strong>{{ trans('applications.gender.gender') }}</strong></div>
                                <div class="col-md-12">
                                    <select name="gender" class="def-input def-select">
                                        <option value="male" selected="selected">{{ trans('applications.gender.male') }}</option>
                                        <option value="female">{{ trans('applications.gender.female') }}</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6 col-sm-6">
                            <div class="form-group">
                                <div class="col-md-12"><strong>{{ trans('applications.nationality') }}</strong></div>
                                <div class="col-md-12">
                                    <select name="nationality" class="def-input def-select countries">
                                        @if(count($countries))
                                            @foreach($countries as $country)
                                                <option value="{{$country->id}}">{{ $country->name_en }}</option>
                                            @endforeach
                                        @endif
                                    </select>
                                </div>
                            </div>
                        </div>

                        <div class="col-md-12 col-sm-12">
                            <div class="form-group">
                                <div class="col-md-12 col-sm-12"><strong>{{ trans('applications.grade') }}</strong></div>
                                <div class="col-md-12 col-sm-12">
                                    <input type="text" name="grade" class="def-input" placeholder="{{ trans('applications.grade') }}">
                                </div>
                            </div>
                        </div>
                        <div class="col-md-12 col-sm-12">
                            <div class="form-group">
                                <div class="col-md-12 col-sm-12"><strong>{{ trans('applications.certificates') }}</strong></div>
                                <div class="col-md-12 col-sm-12">
                                    <input type="file" name="certificates[]" class="def-input" placeholder="{{ trans('applications.certificates') }}" multiple>
                                </div>
                            </div>
                        </div>
                    </div>
                </div><!--/.personal-data -->

                <div class="col-md-12 text-right">
                    <button type="submit" class="def-btn btn-primary">{{ trans('common.save') }}</button>
                </div>
                {{ Form::close() }}
            </div><!--/.container -->
        </div><!--/.form-content -->
    </section>
    @include('frontend.partials._footer')
</body>
</html>