<!-- Footer Starts -->
<footer>
    <div class="copyright sec-q-pad">
        <div class="container">
            <div class="col-md-12 col-sm-12 col-xs-12 left-section text-center"><p>OkTamam All Right Reserved.</p></div>
        </div>
    </div>
</footer>
<!-- Footer Ends -->

<div class="ajaxLoading">
    <div class="image-container">
        <div class="image"><img src="{{ asset('frontend/images/preloader.gif')}}" alt=""></div>
    </div>
</div>

<!-- JavaScripts -->
<!--JQuery-->
<script src="{{ asset('frontend/javascripts/jquery.min.js') }}"></script>
<script src="{{ asset('frontend/javascripts/bootstrap.min.js') }}"></script>
<script src="{{ asset('frontend/javascripts/bootstrap-datepicker.js') }}"></script>
<script src="{{ asset('frontend/javascripts/wow.min.js') }}"></script>
<script src="{{ asset('frontend/javascripts/custom.js') }}"></script>
<script src="{{ asset('frontend/javascripts/select2.min.js') }}"></script>
<script>
    $(document).ready(function(){
        //Select Date from Calender
        $('.datepicker').datepicker({
            startView: 'years',
            format: "yyyy-mm-dd",
            disableTouchKeyboard: true
        });

        // Select Box style with search
        $(".countries").select2();

        // Show Photo before Upload
        function readURL(input) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();
                reader.onload = function (e) {
                    input.closest('.user-photo').find('img').attr('src', e.target.result);
                }
                reader.readAsDataURL(input.files[0]);
            }
        }

        $('input[type="file"]').change(function(){
            var input = this;
            var fileInput = $(this);
            if (input.files && input.files[0]) {
                var reader = new FileReader();
                reader.onload = function (e) {
                    fileInput.closest('.user-photo-wrap').find('img').attr('src', e.target.result);
                }
                reader.readAsDataURL(input.files[0]);
            }
            //readURL(this);
        });

        $('#applicationForm').on('submit', function (e) {
            e.preventDefault();
            $('.ajaxLoading').fadeIn();

            var formData = new FormData($('#applicationForm')[0]);
            $.ajax({
                url: '{{route('home')}}',
                type: 'POST',
                headers: {'X-CSRF-TOKEN': '{{ csrf_token() }}'},
                xhr: function() {
                    var myXhr = $.ajaxSettings.xhr();
                    return myXhr;
                },
                success: function (data) {
                    $('.alert').hide();
                    $('#applicationForm').before(data.msg);
                    $('#applicationForm')[0].reset();
                    $("html, body").animate({ scrollTop: 0 }, "slow");
                    $('.ajaxLoading').fadeOut();
                },
                error: function(data){
                    var errors = data.responseJSON;
                    var errorTxt = '<ul class="alert alert-danger">';
                    $.each(errors, function (index, value) {
                        errorTxt += '<li>'+value+'</li>';
                    });
                    errorTxt += '</ul>';

                    $('.alert').hide();
                    $('#applicationForm').before(errorTxt);
                    $("html, body").animate({ scrollTop: 0 }, "slow");
                    $('.ajaxLoading').fadeOut();
                },
                data: formData,
                cache: false,
                contentType: false,
                processData: false
            });
            return false;
        })
    });
</script>