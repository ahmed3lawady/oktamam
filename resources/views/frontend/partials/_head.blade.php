<head>
    <meta charset="utf-8">
    <meta content="IE=edge" http-equiv="X-UA-Compatible">
    <meta content="width=device-width, initial-scale=1" name="viewport">
    <meta content="text/html" http-equiv="content-type" />
    <meta name="author" content="AhMeDOsAmA" />

    <!--Bootstrap-->
    <link href="{{ asset('frontend/stylesheets/css/bootstrap.css') }}" rel="stylesheet">
    <link href="{{ asset('frontend/stylesheets/css/bootstrap-datepicker.min.css') }}" rel="stylesheet">
    <!-- font-awesome.css -->
    <link href="{{ asset('frontend/stylesheets/css/fontawesome/font-awesome.css') }}" rel="stylesheet">
    <!-- animate.css -->
    <link href="{{ asset('frontend/stylesheets/css/animate.css') }}" rel="stylesheet">
    <!-- User Defined Style -->
    <link href="{{ asset('frontend/stylesheets/css/components.css') }}" rel="stylesheet">
    <link href="{{ asset('frontend/stylesheets/css/select2.min.css') }}" rel="stylesheet">

    <title>Application</title>
</head>