@extends('backend.layouts.template')

@section('title', $title)

@section('content')

    {{ (isset($userData)) ? Form::model($userData, ['url' => ['admin/users', $userData->id], 'method' => 'PUT', 'files'=> true]) : Form::open(['url' => 'admin/users', 'files'=> true]) }}
    <div class="row">
        <!-- left column -->
        <div class="col-md-12">
            <div class="box box-info">
                <div class="box-header">
                    @if(Session::has('msg')){!! Session::get('msg') !!}@endif
                    @if(!empty($errors->all()))
                        <ul class="callout callout-danger">
                            @foreach($errors->all('<li>:message</li>') as $message) {!! $message !!}  @endforeach
                        </ul>
                    @endif
                </div>
                <div class="box-body">
                    <div class="form-group">
                        {{ Form::label('name', trans('users.name').':') }}
                        {{ Form::text('name', old('name'), ['class' => 'form-control', 'placeholder' => trans('users.name')]) }}
                    </div>
                    <div class="form-group">
                        {{ Form::label('email', trans('users.email').':') }}
                        @if(isset($userData->email))
                            {{ Form::email('email', old('email'), ['class' => 'form-control', 'placeholder' => trans('users.email'), 'readonly']) }}
                        @else
                            {{ Form::email('email', old('email'), ['class' => 'form-control', 'placeholder' => trans('users.email')]) }}
                        @endif
                    </div>
                    <div class="form-group">
                        {{ Form::label('password', trans('users.password').':') }}
                        {{ Form::password('password', ['class' => 'form-control', 'placeholder' => trans('users.password')]) }}
                    </div>
                    <div class="clearfix"></div>
                </div>
                <div class="box-footer">
                    {{ Form::submit(trans('common.save'), array('class' => 'btn btn-primary')) }}
                </div>
            </div>
        </div>
    </div>
    {{ Form::close() }}

@stop

@section('scripts')
    <script>
        $(document).ready(function() {
            var city = $('#city_id').val();
            var currRegion = $('#currRegion').val();
            getCityRegions(city, currRegion);

            $('#city_id').on('change', function(){
                city = $(this).val();
                getCityRegions(city, currRegion);
            });

            function getCityRegions(city, currRegion) {
                $.get("/getCityRegions/"+city, function(data, status){
                    var opt = '';
                    $.each(data, function (index, value) {
                        if(index == currRegion){
                            opt += '<option value="'+index+'" selected="selected">'+value+'</option>';
                        }else{
                            opt += '<option value="'+index+'">'+value+'</option>';
                        }
                    });
                    $('#region_id').html(opt);
                });
            }

            $('#addPhone').on('click', function(){
                $('#phones').append('{{ Form::number('phones[]', null, ['class' => 'form-control', 'placeholder' => trans('users.phone')]) }}');
            });
        });
    </script>
@stop
