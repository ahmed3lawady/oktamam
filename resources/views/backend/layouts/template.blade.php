<!DOCTYPE html>
<html>
@include('backend.layouts.partials._head')
<body class="pace-done skin-black fixed">
@include('backend.layouts.partials._header')

<div class="wrapper row-offcanvas row-offcanvas-left">
    <!-- Left side column. contains the logo and sidebar -->
@include('backend.layouts.partials._sidebar')
<!-- Right side column. Contains the navbar and content of the page -->
    <aside class="right-side">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>@yield('title')</h1>
            <ol class="breadcrumb">
                <li><a href="#"><i class="fa fa-dashboard"></i>Control Panel</a></li>
                <li class="active">@yield('title')</li>
            </ol>
        </section>

        <!-- Main content -->
        <section class="content">
            @yield('content')
        </section><!-- /.content -->
    </aside><!-- /.right-side -->
</div><!-- ./wrapper -->

<!-- jQuery 2.0.2 -->
<script src="{{ asset('backend/js/jquery.min.js') }}"></script>
<!-- Bootstrap -->
<script src="{{ asset('backend/js/bootstrap.min.js') }}"></script>
<!-- AdminLTE App -->
<script src="{{ asset('backend/js/AdminLTE/app.js') }}"></script>

@yield('scripts')
</body>
</html>