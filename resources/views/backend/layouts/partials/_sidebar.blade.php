<!-- Left side column. contains the logo and sidebar -->
<aside class="left-side sidebar-offcanvas">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
        <!-- Sidebar user panel -->
        <div class="user-panel">
            <div class="pull-left image">
                <img src="{{ asset('backend/img/avatar3.png') }}" class="img-circle" alt="User Image" />
            </div>
            <div class="pull-left info">
                <p>Hello, {{ str_limit(Auth::user()->name, 10) }}</p>
                <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
            </div>
        </div>

        <!-- sidebar menu: : style can be found in sidebar.less -->
        <ul class="sidebar-menu">
            <li class="treeview @if(Request::is('admin/apps') || Request::is('admin/apps/*')) active @endif">
                <a href="#"><i class="fa fa-file-text"></i><span>{{ trans('applications.applications') }}</span><i class="fa fa-angle-right pull-right"></i></a>
                <ul class="treeview-menu">
                    <li @if(Request::is('admin/apps/create')) class="active" @endif><a href="{{ URL::to('admin/apps/create') }}"><i class="fa fa-angle-double-right"></i> {{ trans('applications.create') }}</a></li>
                    <li @if(Request::is('admin/apps')) class="active" @endif><a href="{{ URL::to('admin/apps') }}"><i class="fa fa-angle-double-right"></i> {{ trans('applications.view-list') }}</a></li>
                </ul>
            </li>
            <li class="treeview @if(Request::is('admin/users') || Request::is('admin/users/*')) active @endif">
                <a href="#"><i class="fa fa-users"></i><span>{{ trans('users.users') }}</span><i class="fa fa-angle-right pull-right"></i></a>
                <ul class="treeview-menu">
                    <li @if(Request::is('admin/users/create')) class="active" @endif><a href="{{ URL::to('admin/users/create') }}"><i class="fa fa-angle-double-right"></i> {{ trans('users.create-user') }}</a></li>
                    <li @if(Request::is('admin/users')) class="active" @endif><a href="{{ URL::to('admin/users') }}"><i class="fa fa-angle-double-right"></i> {{ trans('users.view-users') }}</a></li>
                </ul>
            </li>
            <li><a href="{{ route('logout') }}" ><i class="fa fa-sign-out"></i> <span>{{ trans('users.logout') }}</span></a></li>
        </ul>
    </section>
    <!-- /.sidebar -->
</aside>