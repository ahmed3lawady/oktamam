@extends('backend.layouts.template')

@section('title', $title)

@section('content')

    {{ (isset($appData)) ? Form::model($appData, ['url' => ['admin/apps', $appData->id], 'method' => 'PUT', 'files'=> true]) : Form::open(['url' => 'admin/apps', 'files'=> true]) }}
    <div class="row">
        <!-- left column -->
        <div class="col-md-12">
            <div class="box box-info">
                <div class="box-header">
                    @if(Session::has('msg')){!! Session::get('msg') !!}@endif
                    @if(!empty($errors->all()))
                        <ul class="callout callout-danger">
                            @foreach($errors->all('<li>:message</li>') as $message) {!! $message !!}  @endforeach
                        </ul>
                    @endif
                </div>
                <div class="box-body">
                    <div class="form-group col-md-4">
                        {{ Form::label('FirstName', trans('applications.firstname').':') }}
                        {{ Form::text('firstname', old('firstname'), ['id' => 'FirstName', 'class' => 'form-control', 'placeholder' => trans('applications.firstname')]) }}
                    </div>
                    <div class="form-group col-md-4">
                        {{ Form::label('MiddelName', trans('applications.middelname').':') }}
                        {{ Form::text('middlename', old('middlename'), ['id' => 'MiddelName', 'class' => 'form-control', 'placeholder' => trans('applications.middelname')]) }}
                    </div>
                    <div class="form-group col-md-4">
                        {{ Form::label('SurName', trans('applications.surname').':') }}
                        {{ Form::text('surname', old('surname'), ['id' => 'SurName', 'class' => 'form-control', 'placeholder' => trans('applications.surname')]) }}
                    </div>
                    <div class="form-group col-md-6">
                        {{ Form::label('Birthdate', trans('applications.birthdate').':') }}
                        {{ Form::text('birthdate', old('birthdate'), ['id' => 'Birthdate', 'class' => 'form-control datepicker', 'placeholder' => trans('applications.birthdate')]) }}
                    </div>
                    <div class="form-group col-md-6">
                        {{ Form::label('BirthCountry', trans('cities.birth_country').':') }}
                        <select name="birth_country" class="form-control" id="BirthCountry">
                            @if(count($countries))
                                @foreach($countries as $country)
                                    <option value="{{$country->id}}" @if(isset($appData) && $appData->birth_country == $country->id)selected="selected"@endif>{{ $country->name_en }}</option>
                                @endforeach
                            @endif
                        </select>
                    </div>
                    <div class="form-group col-md-6">
                        {{ Form::label('gender', trans('applications.gender.gender').':') }}
                        {{ Form::select('gender', ['male' => trans('applications.gender.male'), 'female' => trans('applications.gender.female')], old('gender'), ['id' => 'gender', 'class' => 'form-control']) }}
                    </div>
                    <div class="form-group col-md-6">
                        {{ Form::label('Nationality', trans('applications.nationality').':') }}
                        <select name="nationality" class="form-control" id="Nationality">
                            @if(count($countries))
                                @foreach($countries as $country)
                                    <option value="{{$country->id}}" @if(isset($appData) && $appData->nationality == $country->id)selected="selected"@endif>{{ $country->name_en }}</option>
                                @endforeach
                            @endif
                        </select>
                    </div>
                    <div class="form-group col-md-6">
                        <div class="col-xs-12 text-center">
                            <img src="{{ (isset($appData) && $appData->photo) ? route('image.small', [$appData->photo]) : asset('backend/img/no-image-found.jpg') }}" height="100" />
                        </div>
                        {{ Form::label('Photo', trans('applications.image').':') }}
                        {{ Form::file('photo', array('id' => 'Photo', 'class' => 'form-control', 'placeholder' => trans('applications.image'))) }}
                    </div>
                    <div class="form-group col-md-6">
                        <div class="col-xs-12 text-center">
                            <img src="{{ (isset($appData) && $appData->passport) ? route('image.small', [$appData->passport]) : asset('backend/img/no-image-found.jpg') }}" height="100" />
                        </div>
                        {{ Form::label('Passport', trans('applications.passport').':') }}
                        {{ Form::file('passport', array('id' => 'Passport', 'class' => 'form-control', 'placeholder' => trans('applications.passport'))) }}
                    </div>
                    <div class="form-group col-md-6">
                        {{ Form::label('Grade', trans('applications.grade').':') }}
                        {{ Form::text('grade', old('grade'), ['id' => 'Grade', 'class' => 'form-control', 'placeholder' => trans('applications.grade')]) }}
                    </div>
                    <div class="form-group col-md-6">
                        {{ Form::label('Certificates', trans('applications.certificates').':') }}
                        {{ Form::file('certificates[]', ['id' => 'Certificates', 'class' => 'form-control', 'placeholder' => trans('applications.certificates'), 'multiple']) }}
                    </div>
                    @if(isset($appData) && count($appData->certificates))
                        <div class="col-md-12">
                            {{ Form::label('Certificates', trans('applications.certificates').':') }}
                            <ul class="list-group">
                                @foreach($appData->certificates as $certif)
                                    <li style="float: left;margin-right: 5px;list-style: none"><img src="{{route('image.small', [$certif->photo])}}" class="img-thumbnail" style="height: 200px"></li>
                                @endforeach
                            </ul>
                        </div>
                    @endif
                    <div class="clearfix"></div>
                </div>
                <div class="box-footer">
                    {{ Form::submit(trans('common.save'), array('class' => 'btn btn-primary')) }}
                </div>
            </div>
        </div>
    </div>
    {{ Form::close() }}

@stop

@section('styles')
    <link href="{{ asset('backend/css/bootstrap-datepicker.min.css') }}" rel="stylesheet">
@append

@section('scripts')
    <script src="{{ asset('backend/js/bootstrap-datepicker.js') }}"></script>
    <script>
        $(document).ready(function() {
            $('.datepicker').datepicker({
                startView: 'years',
                format: "yyyy-mm-dd",
                disableTouchKeyboard: true
            });
        });
    </script>
@append
