@extends('backend.layouts.template')

@section('title'){{ trans('applications.list') }} @stop

@section('content')

    <div class="row">
        <div class="col-xs-12">
            <div class="box">
                <div class="box-header">
                    @if(Session::has('msg')){!! Session::get('msg') !!}@endif
                </div><!-- /.box-header -->
                <div class="box-body table-responsive">
                    <table id="example1" class="table table-bordered table-striped text-center">
                        <thead>
                        <tr>
                            <th>{{ trans('applications.code') }}</th>
                            <th>{{ trans('applications.fullname') }}</th>
                            <th>{{ trans('applications.image') }}</th>
                            <th>{{ trans('common.update') }}</th>
                            <th>{{ trans('common.delete') }}</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($allApps as $app)
                            <tr>
                                <td>{{ $app->code }}</td>
                                <td>{{ $app->fullname() }}</td>
                                <td><img src="{{ ($app->photo) ? route('image.thumbnail', [$app->photo]) : asset('backend/img/no-image-found.jpg') }}" height="100" /></td>
                                <td><a href="{{ URL::to('admin/apps/'.$app->id.'/edit') }}"><i class="fa fa-pencil" aria-hidden="true"></i></a></td>
                                <td>
                                    {{ Form::open(array('url' => array('admin', $app->id), 'method' => 'DELETE')) }}
                                    <button type="submit" class="btn btn-danger btn-sm" onclick="confirm('{{trans('common.delete-confirm')}}');"><i class="fa fa-trash-o" aria-hidden="true"></i></button>
                                    {{ Form::close() }}
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                        <tfoot>
                        <tr>
                            <th>{{ trans('applications.id') }}</th>
                            <th>{{ trans('applications.fullname') }}</th>
                            <th>{{ trans('applications.image') }}</th>
                            <th>{{ trans('common.update') }}</th>
                            <th>{{ trans('common.delete') }}</th>
                        </tr>
                        </tfoot>
                    </table>
                </div><!-- /.box-body -->
            </div><!-- /.box -->
        </div>
    </div>

@stop

@section('scripts')
    {{ Html::style('backend/css/datatables/dataTables.bootstrap.css') }}
    {{ Html::script('backend/js/plugins/datatables/jquery.dataTables.js') }}
    {{ Html::script('backend/js/plugins/datatables/dataTables.bootstrap.js') }}
    <script type="text/javascript">
        $(function() {
            $("#example1").dataTable();
        });
    </script>
@stop